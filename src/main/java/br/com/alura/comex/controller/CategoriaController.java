package br.com.alura.comex.controller;


import br.com.alura.comex.model.Categoria;
import br.com.alura.comex.repository.CategoriaRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;

import javax.transaction.Transactional;
import javax.validation.Valid;
import java.net.URI;
import java.util.List;

import static org.springframework.http.ResponseEntity.created;
import static org.springframework.web.servlet.support.ServletUriComponentsBuilder.fromCurrentRequestUri;

@RestController
@RequestMapping("/categorias")
public class CategoriaController {

	@Autowired
	private CategoriaRepository categoriaRepository;



	@Transactional
	@PostMapping
	public ResponseEntity<Categoria> cadastrar(@RequestBody @Valid Categoria categoria, UriComponentsBuilder uriBuilder) {
		categoriaRepository.save(categoria);
		URI uri = uriBuilder.path("/categorias/{id}").buildAndExpand(categoria.getId()).toUri();
		return ResponseEntity.created(uri).body(categoria);
	}
}

//@ResponseStatus(value = HttpStatus.CREATED)
//@RequestMapping(method = RequestMethod.POST, produces = "application/json")
//public ResponseEntity<Categoria> save(@RequestBody @Valid Categoria categoria) {
//	return created(fromCurrentRequestUri().path(service.saveOrUpdate(categoria).getId().toString()).build().toUri()).body(categoria);
//}
//}
//






